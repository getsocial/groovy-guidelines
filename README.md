### Groovy Guidelines
The style guidelines use the following external style guides in priority order, along with
a customizations that aren't explicit in those guides.

1. Groovy Style Guidlines (highest priority):
   http://www.groovy-lang.org/style-guide.html
2. Java Style Guidelines (when not in conflict with above):
   https://google-styleguide.googlecode.com/svn/trunk/javaguide.html

Further Customizations:

* Function calls should use parentheses:

```groovy
logging.captureStandardOutput(LogLevel.INFO)  // CORRECT
logging.captureStandardOutput LogLevel.INFO   // WRONG
```

* Configure calls do not use parentheses:

```groovy
project.exec {
    executable 'j2objc'              // CORRECT
    args '-sourcepath', sourcepath   // CORRECT

    executable('j2objc')             // WRONG
    args('-sourcepath', sourcepath)  // WRONG
}
```

* Explicit Types instead of 'def':

```groovy
String message = 'a message'  // CORRECT
def message = 'a message'     // WRONG

// This also applies for '->' iterators:
translateArgs.each { String arg ->  // CORRECT
translateArgs.each { arg ->         // WRONG
```

* GString curly braces only for methods or object members:

```groovy
String message = "the count is $count"           // CORRECT
String message = "the count is ${object.count}"  // CORRECT
String message = "the count is ${method()}"      // CORRECT

String message = "the count is ${count}"         // WRONG
String message = "the count is $method()"        // WRONG
String message = "the count is $object.count"    // incorrect as it only prints "$object"
```

* Single quotes for non-GString, i.e. no string interpolation:

```groovy
String message = 'the count is negative'  // CORRECT
String message = "the count is negative"  // WRONG - only needed for $var interpolation
```

* Regexes should be written and displayed as
[Slashy Strings](http://docs.groovy-lang.org/latest/html/documentation/#_slashy_string):

```groovy
String regex = /dot-star:.*, forward-slash:\/, newline:\n/
logger.debug('Regex is: ' + Utils.escapedSlashyString(regex))

// Debug log: '/dot-star:.*, forward-slash:\/, newline:\n/'
```